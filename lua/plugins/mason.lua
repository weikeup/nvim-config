return {
  "williamboman/mason.nvim",
  dependencies = {
    {"williamboman/mason-lspconfig.nvim"}
  },
  event = "VeryLazy",
  config = function()
    require("mason").setup()
    require("mason-lspconfig").setup()

    require("mason-lspconfig").setup_handlers {
      -- The first entry (without a key) will be the default handler
      -- and will be called for each installed server that doesn't have
      -- a dedicated handler.
      function (server_name) -- default handler (optional)
        require("lspconfig")[server_name].setup {}
      end,
      -- Next, you can provide a dedicated handler for specific servers.
      ["lua_ls"] = function ()
        require("lspconfig").lua_ls.setup {
          single_file_support = true
        }
      end
    }
  end
}
