vim.cmd.colorscheme("tokyonight-night")
vim.api.nvim_set_hl(0, "TelescopeNormal", { bg = "none" })
vim.api.nvim_set_hl(0, "TelescopeBorder", { bg = "none" })
vim.o.rnu = true
vim.o.number = true
